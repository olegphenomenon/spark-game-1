/**
 * (c) Facebook, Inc. and its affiliates. Confidential and proprietary.
 */

//==============================================================================
// Welcome to scripting in Spark AR Studio! Helpful links:
//
// Scripting Basics - https://fb.me/spark-scripting-basics
// Reactive Programming - https://fb.me/spark-reactive-programming
// Scripting Object Reference - https://fb.me/spark-scripting-reference
// Changelogs - https://fb.me/spark-changelog
//
// For projects created with v87 onwards, JavaScript is always executed in strict mode.
//==============================================================================

// How to load in modules
const Scene = require('Scene');
const Animation = require('Animation');

let fps = 30;
let h = 0.01;

let octahedron;
let randomNumber;
let face = Scene.root.find('face');
let text = Scene.root.find('text');
let score = 0;

function random(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const moveDownDriver = Animation.timeDriver({
    durationMilliseconds: fps,
    loopCount: Infinity,
    mirror: false
});
moveDownDriver.onAfterIteration().subscribe(function() { moveDown(); });

function moveDown() {
        if (octahedron == null) {
            randomNumber = random(0, 2);
            octahedron = Scene.root.find('Octahedron' + randomNumber);
        }
        
        if (octahedron.transform.y.pinLastValue() < -0.3) {
            octahedron.transform.y = 0.3;
            octahedron = null;
            return;
        }

        octahedron.transform.y = octahedron.transform.y.pinLastValue() - h;
        octahedron.transform.x = face.transform.x.pinLastValue();

        let x = octahedron.transform.x.pinLastValue();
        let y = octahedron.transform.y.pinLastValue();

        let cylinder_y = -0.15;
        let cylinder_h = 0.12;
        if(y < cylinder_y + cylinder_h && y > cylinder_y - cylinder_h) {

            score += 1;
            text.text = score + "";
        }

    }

moveDownDriver.start();